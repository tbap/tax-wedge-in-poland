#!/usr/bin/Rscript

social.tax.limit = 112380 # dochód powyżej którego nie nalicza się składki emerytalnej i rentowej

social.employer.pension = 0.0976 # emerytalna
social.employer.incapacity = 0.065 # rentowa
social.employer.sickness.insurance = 0.0245
social.employer.fgsp = 0.001 # Fundusz Gwarantowanych Świadczeń Pracowniczych
social.employer.accident.insurance = 0.0193 # ubezpieczenie wypadkowe

social.employee.pension = 0.0976 # emerytalna
social.employee.incapacity = 0.015 # rentowa
social.employee.sickness.insurance = 0.0245 # chorobowa

social.entrepreneur.pension = 661.92
social.entrepreneur.fp = 55.07

# NFZ:
health.premium.rate = 0.09 
health.entrepreneur = 270.40 

pit.cost = 111.25 # koszty uzyskania
pit.threshold = 85528 # dochód od którego naliczany jest drugi próg
pit.rate1 = 0.18
pit.rate2 = 0.32
pit.health.deduction.rate = 0.0775 # odliczenie NFZ
pit.allowance = 46.33 # kwota wolna od podatku

# helper function for unit tests
assertEquals = function(expected, actual, message=NULL) {
  if (!isTRUE(all.equal(expected, actual))) {
    stop(sprintf("Assertion failed: expected %f does not match actual %f", expected, actual ))
  }
}

social.employer = function(gross) {
  base = min(social.tax.limit/12, gross)
  return (round(base * (social.employer.pension + social.employer.incapacity) 
    + gross * (social.employer.sickness.insurance + social.employer.accident.insurance + social.employer.fgsp), 2))
}

social.employee = function(gross) {
  base = min(social.tax.limit/12, gross)
  return (round(base * (social.employee.pension + social.employee.incapacity) + gross * social.employee.sickness.insurance, 2))
}

social = function(gross) {
  return (social.employer(gross) + social.employee(gross))
}

health = function(gross) {
  return (round(health.premium.rate * (gross - social.employee(gross)), 2))
}

pit.tax.before.reductions = function(base) {  
  threshold = pit.threshold/12
  return (pit.rate1 * min(base, threshold) + pit.rate2 * max(0, base - threshold) - pit.allowance )
}

pit = function(gross) {
  base = gross - social.employee(gross) - pit.cost
  tax.before.deductions = pit.tax.before.reductions(base)
  health.deduction = pit.health.deduction.rate * (gross - social.employee(gross))
  tax = max(0, tax.before.deductions - health.deduction)
  return (round(tax))                                      
}

total.cost = function(gross) {
  return (gross + social.employer(gross))
}

entrepreneur.health = function(gross) {
  return (health.entrepreneur)
}

entrepreneur.social = function(gross) {
  return (social.entrepreneur.pension + social.entrepreneur.fp)
}

entrepreneur.pit = function(gross) {
  base = gross - entrepreneur.social(gross)
  tax.before.deductions = pit.tax.before.reductions(base)
  health.deduction = pit.health.deduction.rate/health.premium.rate * entrepreneur.health(gross)
  tax = max(0, tax.before.deductions - health.deduction)
  return (round(tax))                                      
}

entrepreneur.total.cost = function(gross) {
  return (gross)
}

tests = function() {
  # zerowy próg:
  assertEquals(0, pit(0))
  assertEquals(0, pit(100))
  
  # pierwszy próg
  assertEquals(663.68, social.employer(3200))
  assertEquals(438.72, social.employee(3200))
  assertEquals(1102.40, social(3200))
  assertEquals(248.52, health(3200))
  assertEquals(217, pit(3200))
  assertEquals(3863.68, total.cost(3200))
   
  # drugi próg:
  assertEquals(2060.35, social.employer(12000))
  assertEquals(1348.50, social.employee(12000))
  assertEquals(958.63, health(12000))
  #assertEquals(17106/12, pit(12000)) # brak pewności co do danych, test w obecnej formie nie przechodzi.
  
  # samozatrudnienie:
  assertEquals(270.40, entrepreneur.health(0))
  assertEquals(661.92 + 55.07, entrepreneur.social(0))
  assertEquals(0, entrepreneur.pit(0))
  
  assertEquals(270.40, entrepreneur.health(4000))
  assertEquals(661.92 + 55.07, entrepreneur.social(4000))
  assertEquals(498.71, pit.tax.before.reductions(3028))
  assertEquals(312, entrepreneur.pit(4000))
}

tests() 