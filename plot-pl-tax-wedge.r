#!/usr/bin/Rscript

require(ggplot2)
require(reshape2)
require(plyr)
require(scales) 

source("functions.r")

prepareData.employee <- function() {
  x = seq(0, 20000, by=100)
  total.costs = sapply(x, total.cost)
  social = sapply(x, social)/total.costs
  health = sapply(x, health)/total.costs
  tax = sapply(x, pit)/total.costs
  
  data <- data.frame(x, tax, social, health)
  data=melt(data, id="x")
  return(data)
}

prepareData.entrepreneur <- function() {
  x = seq(100, 20000, by=100)
  total.costs = sapply(x, entrepreneur.total.cost)
  social = sapply(x, entrepreneur.social)/total.costs
  health = sapply(x, entrepreneur.health)/total.costs
  tax = sapply(x, entrepreneur.pit)/total.costs
  
  data <- data.frame(x, tax, health, social)
  data=melt(data, id="x")
  return(data)
}

plot.taxwedge = function(data, title, ylim) {
  wage.min = 1680
  wage.mean = 4000
  
  ggplot(data,aes(x = x, y = value, fill = variable, order = desc(variable))) + 
    geom_area( position = 'stack',  size=.2, alpha=.7  ) +
    scale_y_continuous(name = "klin podatkowy", breaks=seq(0, 2, by=0.2)) +
    scale_x_continuous(name = "dochód miesięczny brutto (PLN)", trans=sqrt_trans(), breaks=c(0, wage.min,  wage.mean, 2*wage.mean, 3*wage.mean, 4*wage.mean, 20000)) +
    scale_fill_discrete(name="", breaks=c("tax", "social", "health"), labels=c("PIT", "ZUS", "NFZ")) + 
    coord_cartesian(ylim=ylim) +
    ggtitle(title)    
}

plot.entrepreneur = function() {
  data = prepareData.entrepreneur() 
  plot.taxwedge(data, "klin podatkowy przedsiębiorcy", c(0, 2))
}

plot.employee = function() {
  data = prepareData.employee() 
  plot.taxwedge(data, "klin podatkowy pracownika etatowego", c(0, 1))
}

plot.entrepreneur();
#plot.employee();