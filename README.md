# README #

This is a script generating a plot of tax wedge in Poland depending on the gross income.
Example: ![tax-wedge.png](https://bitbucket.org/repo/BdER6p/images/4099494493-tax-wedge.png)

### What is this repository for? ###

* To show the code, maintain it and include improvements

### How do I get set up? ###

* In order to run the script, you need [R project](http://www.r-project.org/) installed. The script uses reshape and ggplot2 packages.

### Contribution guidelines ###

* I wrote a simple test method that has to pass before a plot is generated.